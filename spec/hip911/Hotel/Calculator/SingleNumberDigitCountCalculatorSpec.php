<?php

namespace spec\hip911\Hotel\Calculator;

use hip911\Hotel\Calculator\DigitCountCalculator;
use hip911\Hotel\Calculator\SingleNumberDigitCountCalculator;
use hip911\Hotel\ValueObject\DigitCount;
use PhpSpec\ObjectBehavior;

class SingleNumberDigitCountCalculatorSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldImplement(DigitCountCalculator::class);
        $this->shouldHaveType(SingleNumberDigitCountCalculator::class);
    }

    function it_gets_the_DigitCount_for_a_simple_number()
    {
        $expected = $this->constructExpected('1','1','1','1','1','1','1','1','1','1');
        $this->calculate('1234567890')->getDigitCounts()->shouldReturn($expected->getDigitCounts());

        $expected2 = $this->constructExpected('3','1','1','1','1','0','0','1','1','1');
        $this->calculate('1234007890')->getDigitCounts()
            ->shouldReturn($expected2->getDigitCounts());
    }

    function constructExpected()
    {
        $argv = func_get_args();
        return new DigitCount([$argv[0],$argv[1],$argv[2],$argv[3],$argv[4],$argv[5],$argv[6],$argv[7],$argv[8],$argv[9]]);
    }
}
