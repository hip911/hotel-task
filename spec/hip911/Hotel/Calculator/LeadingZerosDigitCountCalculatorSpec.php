<?php

namespace spec\hip911\Hotel\Calculator;

use hip911\Hotel\Calculator\DigitCountCalculator;
use hip911\Hotel\Calculator\LeadingZerosDigitCountCalculator;
use hip911\Hotel\ValueObject\DigitCount;
use PhpSpec\ObjectBehavior;

class LeadingZerosDigitCountCalculatorSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldImplement(DigitCountCalculator::class);
        $this->shouldHaveType(LeadingZerosDigitCountCalculator::class);
    }

    function it_passes_on_any_amounts_of_nines()
    {
        $expected = $this->constructExpected('1','0','0','0','0','0','0','0','0','0');
        $this->calculate('9')->getDigitCounts()->shouldReturn($expected->getDigitCounts());
        $expected = $this->constructExpected('11','0','0','0','0','0','0','0','0','0');
        $this->calculate('99')->getDigitCounts()->shouldReturn($expected->getDigitCounts());
        $expected = $this->constructExpected('111','0','0','0','0','0','0','0','0','0');
        $this->calculate('999')->getDigitCounts()->shouldReturn($expected->getDigitCounts());
    }

    function it_passes_on_other_than_9s()
    {
        $this->calculate('1000')->getDigitCounts()->shouldEqual($this->calculate('999')->getDigitCounts());
    }

    function constructExpected()
    {
        $argv = func_get_args();
        return new DigitCount([$argv[0],$argv[1],$argv[2],$argv[3],$argv[4],$argv[5],$argv[6],$argv[7],$argv[8],$argv[9]]);
    }
}
