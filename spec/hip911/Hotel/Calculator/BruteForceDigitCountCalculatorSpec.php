<?php

namespace spec\hip911\Hotel\Calculator;

use hip911\Hotel\Calculator\BruteForceDigitCountCalculator;
use hip911\Hotel\Calculator\DigitCountCalculator;
use hip911\Hotel\Calculator\SingleNumberDigitCountCalculator;
use hip911\Hotel\ValueObject\DigitCount;
use PhpSpec\ObjectBehavior;

class BruteForceDigitCountCalculatorSpec extends ObjectBehavior
{
    function let()
    {
        $calculator = new SingleNumberDigitCountCalculator();
        $this->beConstructedWith($calculator);
    }

    /** @mixin BruteForceDigitCountCalculator */
    function it_is_initializable()
    {
        $this->shouldImplement(DigitCountCalculator::class);
        $this->shouldHaveType(BruteForceDigitCountCalculator::class);
    }

    function it_calculates_the_DigitCount_for_small_numbers()
    {
        $expected = $this->constructExpected('1','2','1','1','1','1','1','1','1','1');
        $this->calculate('10')->getDigitCounts()->shouldReturn($expected->getDigitCounts());

        $expected = $this->constructExpected('31','140','41','40','40','40','40','40','40','40');
        $this->calculate('200')->getDigitCounts()->shouldReturn($expected->getDigitCounts());
    }

    function constructExpected()
    {
        $argv = func_get_args();
        return new DigitCount([$argv[0],$argv[1],$argv[2],$argv[3],$argv[4],$argv[5],$argv[6],$argv[7],$argv[8],$argv[9]]);
    }
}
