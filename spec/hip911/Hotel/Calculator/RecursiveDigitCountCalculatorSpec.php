<?php

namespace spec\hip911\Hotel\Calculator;

use hip911\Hotel\Calculator\BaseValueDigitCountCalculator;
use hip911\Hotel\Calculator\BruteForceDigitCountCalculator;
use hip911\Hotel\Calculator\DigitCountCalculator;
use hip911\Hotel\Calculator\LeadingZerosDigitCountCalculator;
use hip911\Hotel\Calculator\RecursiveDigitCountCalculator;
use hip911\Hotel\Calculator\SingleNumberDigitCountCalculator;
use hip911\Hotel\ValueObject\DigitCount;
use PhpSpec\ObjectBehavior;

class RecursiveDigitCountCalculatorSpec extends ObjectBehavior
{
    function let()
    {
        $bvCalculator = new BaseValueDigitCountCalculator();
        $lzCalculator = new LeadingZerosDigitCountCalculator();
        $this->beConstructedWith($bvCalculator,$lzCalculator);
    }
    /** @mixin RecursiveDigitCountCalculator */
    function it_is_initializable()
    {
        $this->shouldImplement(DigitCountCalculator::class);
        $this->shouldHaveType(RecursiveDigitCountCalculator::class);
    }

    function it_passes_on_any_amounts_of_nines()
    {
        $this->passes('9');
        $this->passes('99');
        $this->passes('999');
    }

    function it_passes_on_less_than_10()
    {
        $this->passes('0');
        $this->passes('8');
    }

    function it_passes_on_less_than_20()
    {
        $this->passes('299');
        $this->passes('210');
        $this->passes('110');
    }

    function it_passes_on_numbers_around_100()
    {
        for( $i = 99; $i < 120; $i++) {
            $this->passes((string)$i);
        }
    }

    function it_passes_on_numbers_around_1000()
    {
        for( $i = 999; $i < 1011; $i++) {
            $this->passes((string)$i);
        }
    }

    function it_passes_on_numbers_around_2000()
    {
        for( $i = 1999; $i < 2011; $i++) {
            $this->passes((string)$i);
        }
    }

    function it_passes_on_numbers_around_1100()
    {
        for( $i = 1099; $i < 1111; $i++) {
            $this->passes((string)$i);
        }
    }

    function it_passes_on_numbers_around_2100()
    {
        for( $i = 2099; $i < 2111; $i++) {
            $this->passes((string)$i);
        }
    }

    /**
     * @return DigitCount
     */
    private function passes($stringNumber)
    {
        $control = (new BruteForceDigitCountCalculator(new SingleNumberDigitCountCalculator()))->calculate($stringNumber);
        $this->calculate($stringNumber)->getDigitCounts()->shouldReturn($control->getDigitCounts());
        return;
    }
}
