<?php

namespace spec\hip911\Hotel\ValueObject;

use hip911\Hotel\ValueObject\DigitCount;
use PhpSpec\ObjectBehavior;

class DigitCountSpec extends ObjectBehavior
{
    /** @mixin DigitCount */
    function it_is_initializable()
    {
        $this->shouldHaveType(DigitCount::class);
    }

    function it_sets_all_digit_counts_to_zero_on_initialization_with_no_parameters()
    {
        $counts = $this->getDigitCounts();
        $counts->shouldHaveCount(10);
        for ($i=0;$i<10;$i++) {
            $counts->shouldHaveKeyWithValue($i,'0');
        }
    }

    function it_accepts_a_count_for_a_digit_on_initialization_with_parameters()
    {
        $override = ['0'=>'1','5'=>'3','9'=>'2'];
        $this->beConstructedWith($override);
        $counts = $this->getDigitCounts();
        $counts->shouldHaveKeyWithValue('0','1');
        $counts->shouldHaveKeyWithValue('5','3');
        $counts->shouldHaveKeyWithValue('9','2');
    }

    function it_can_be_constructed_with_same_value_for_all_digits()
    {
        $this->beConstructedThrough('createWithAllSame',['10']);
        $counts = $this->getDigitCounts();
        for ($i=0;$i<10;$i++) {
            $counts->shouldHaveKeyWithValue($i,'10');
        }
    }

    function it_can_be_added_to_another_DigitCount(DigitCount $another)
    {
        $this->beConstructedThrough('createWithAllSame',['5']);
        $another->getDigitCounts()->willReturn(['0'=>1,'1'=>2,'2'=>3,'3'=>4,'4'=>5,'5'=>6,'6'=>7,'7'=>8,'8'=>9,'9'=>10]);
        $added = $this->add($another);
        $counts = $added->getDigitCounts();
        for ($i=0;$i<10;$i++) {
            $counts->shouldHaveKeyWithValue($i,bcadd($i,6));
        }
    }

    function it_accepts_another_DigitCount_for_subtraction(DigitCount $another)
    {
        $this->beConstructedThrough('createWithAllSame',['5']);
        $another->getDigitCounts()->willReturn(['0'=>6,'1'=>7,'2'=>8,'3'=>9,'4'=>10,'5'=>11,'6'=>12,'7'=>13,'8'=>14,'9'=>15]);
        $added = $this->subtract($another);
        $counts = $added->getDigitCounts();
        for ($i=0;$i<10;$i++) {
            $counts->shouldHaveKeyWithValue($i,bcadd(-$i,-1));
        }
    }

    function it_accepts_an_integer_for_multiplication()
    {
        $int = rand(1,100);
        $this->beConstructedThrough('createWithAllSame',['5']);
        $added = $this->multiply($int);
        $counts = $added->getDigitCounts();
        for ($i=0;$i<10;$i++) {
            $counts->shouldHaveKeyWithValue($i,bcmul('5',$int));
        }
    }
}
