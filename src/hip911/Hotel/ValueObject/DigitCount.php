<?php

namespace hip911\Hotel\ValueObject;

class DigitCount
{
    private $digitCounts = [
        '0'=>'0',
        '1'=>'0',
        '2'=>'0',
        '3'=>'0',
        '4'=>'0',
        '5'=>'0',
        '6'=>'0',
        '7'=>'0',
        '8'=>'0',
        '9'=>'0',
    ];

    public function __construct(array $override = [])
    {
        //TODO validate keys
        foreach ($override as $key => $value) {
            $this->digitCounts[$key] = bcadd($value,0);
        }
    }

    public function getDigitCounts()
    {
        return $this->digitCounts;
    }

    public static function createWithAllSame($overrideAll = '0')
    {
        $override = [];
        for ($i=0;$i<10;$i++) {
            $override[$i] = $overrideAll;
        }

        return new static($override);
    }

    /**
     * @param DigitCount $another
     * @return static
     */
    public function add(DigitCount $another)
    {
        $anotherDigitcounts = $another->getDigitCounts();
        $tmpDigitCounts = $this->digitCounts;
        foreach ($anotherDigitcounts as $key => $value) {
            $tmpDigitCounts[$key] = bcadd($tmpDigitCounts[$key],$value);
        }

        return new static($tmpDigitCounts);
    }

    /**
     * @param DigitCount $another
     * @return static
     */
    public function subtract(DigitCount $another)
    {
        $anotherDigitcounts = $another->getDigitCounts();
        $tmpDigitCounts = $this->digitCounts;
        foreach ($anotherDigitcounts as $key => $value) {
            $tmpDigitCounts[$key] = bcsub($tmpDigitCounts[$key],$value);
        }

        return new static($tmpDigitCounts);
    }

    /**
     * @param integer $integer
     * @return static
     */
    public function multiply($integer)
    {
        $tmpDigitCounts = $this->digitCounts;
        foreach ($this->digitCounts as $key => $value) {
            $tmpDigitCounts[$key] = bcmul($tmpDigitCounts[$key],$integer);
        }

        return new static($tmpDigitCounts);
    }
}
