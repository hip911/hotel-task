<?php

namespace hip911\Hotel\Calculator;

use hip911\Hotel\ValueObject\DigitCount;

class BruteForceDigitCountCalculator implements DigitCountCalculator
{

    /**
     * @var SingleNumberDigitCountCalculator
     */
    private $calculator;

    /**
     * BruteForceDigitCountCalculator constructor.
     * @param SingleNumberDigitCountCalculator $calculator
     */
    public function __construct(SingleNumberDigitCountCalculator $calculator)
    {
        $this->calculator = $calculator;
    }

    /**
     * @param string $number
     * @return DigitCount
     */
    public function calculate($number)
    {
        $total = DigitCount::createWithAllSame();
        for($i=1; $i <= $number; $i++) {
            $current = $this->calculator->calculate((int)$i);
            $total = $total->add($current);
        }

        return $total;
    }
}
