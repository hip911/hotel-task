<?php

namespace hip911\Hotel\Calculator;

use hip911\Hotel\ValueObject\DigitCount;

class LeadingZerosDigitCountCalculator implements DigitCountCalculator
{
    /**
     * @param string $number
     * @return DigitCount
     */
    public function calculate($number)
    {
        $whichPowOf10 = strlen($number) - 1;
        if(!preg_match('/^[9]*$/',$number)) {
            $whichPowOf10 -= 1;
        }

        $takeOff = 0;
        for ( $x = $whichPowOf10; $x >= 0; $x--) {
            $takeOff = bcadd($takeOff,bcpow(10,$x));
        }
        return new DigitCount(['0' => $takeOff]);
    }
}
