<?php

namespace hip911\Hotel\Calculator;

use hip911\Hotel\ValueObject\DigitCount;

class BaseValueDigitCountCalculator implements DigitCountCalculator
{
    /**
     * @inheritDoc
     */
    public function calculate($number)
    {
        $whichPowOf10 = strlen($number) - 1;

        $baseValue = $this->internalCalculate($whichPowOf10);
        return DigitCount::createWithAllSame($baseValue);
    }

    private function internalCalculate($whichPowOf10)
    {
        $whichPowOf10 = (string)$whichPowOf10;
        if($whichPowOf10 === '0') return '1';

        return bcadd(
            bcmul(
                $this->internalCalculate(bcsub($whichPowOf10,'1')),
                '10'
            ),
            bcpow(
                '10',
                $whichPowOf10
            )
        );
    }
}
