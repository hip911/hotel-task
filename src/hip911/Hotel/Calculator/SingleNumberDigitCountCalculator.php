<?php

namespace hip911\Hotel\Calculator;

use hip911\Hotel\ValueObject\DigitCount;

class SingleNumberDigitCountCalculator implements DigitCountCalculator
{
    /**
     * @param string $number
     * @return DigitCount
     */
    public function calculate($number)
    {
        $digits = str_split($number);
        $grouppedDigits = DigitCount::createWithAllSame()->getDigitCounts();
        foreach ($digits as $digitValue) {
            $grouppedDigits[$digitValue] = bcadd($grouppedDigits[$digitValue],1);
        }

        return new DigitCount($grouppedDigits);
    }
}
