<?php

namespace hip911\Hotel\Calculator;

use hip911\Hotel\ValueObject\DigitCount;

/**
 * Interface DigitCountCalculator
 * @package hip911\Hotel\Calculator
 */
interface DigitCountCalculator
{
    /**
     * @param string $number
     * @return DigitCount
     */
    public function calculate($number);
}