<?php

namespace hip911\Hotel\Calculator;

use hip911\Hotel\ValueObject\DigitCount;

class RecursiveDigitCountCalculator implements DigitCountCalculator
{
    private $bvCalculator;
    private $lzCalculator;

    public function __construct(BaseValueDigitCountCalculator $bvCalculator,LeadingZerosDigitCountCalculator $lzCalculator)
    {
        $this->bvCalculator = $bvCalculator;
        $this->lzCalculator = $lzCalculator;
    }

    /**
     * @param string $number
     * @return DigitCount
     */
    public function calculate($number)
    {
        return $this->internalCalculate($number);
    }


    /**
     * @param string $number
     * @param bool $withOutZeros
     * @return DigitCount
     */
    private function internalCalculate($number, $withOutZeros = true)
    {
        $whichPowOf10 = strlen($number) - 1;

        /* if number is a single digit */
        if( $whichPowOf10 == 0 ) {

            $simpleFinal = (new BruteForceDigitCountCalculator(new SingleNumberDigitCountCalculator()))->calculate($number);

            if(!$withOutZeros) {
                $simpleFinal = $simpleFinal->add(new DigitCount(['0'=>'1']));
            }

            return $simpleFinal;
        }

        /* if number contains only nines */
        if(preg_match('/^[9]*$/',$number)) {
            $baseValueDigitCount = $this->bvCalculator->calculate($number);

            if($withOutZeros) {
                $leadingZeros = $this->lzCalculator->calculate($number);
                $baseValueDigitCount = $baseValueDigitCount->subtract($leadingZeros);
            }

            return $baseValueDigitCount;
        }

        /* all other cases */
        $allNines = implode('', array_fill(0, $whichPowOf10, '9'));
        $digitCountForRoundedDownNines = $this->internalCalculate($allNines,false);

        $firstDigit = substr($number,0,1);
        $remaining = bcsub(
            $number,
            bcmul(
                bcadd($allNines,1),
                $firstDigit
            )
        );
        $remainingWhichPowOf10 = strlen($remaining) - 1;
        $roundingDistance = strlen($number) - strlen($remaining);

        /* STEPS */

        /* add the amount of nines $firstDigit times to the total */
        $final = $digitCountForRoundedDownNines->multiply($firstDigit);

        /* pad the missing first digits up to the floored amount */
        for( $i = 1; $i < $firstDigit; $i++) {
            $stuff2 = new DigitCount([$i => pow(10,$whichPowOf10)]);
            $final = $final->add($stuff2);
        }

        /* add the first left padded digits up to the $remaining */
        $padding = str_split(substr($number,0,$roundingDistance));
        foreach ($padding as $item) {
            $stuff = new DigitCount([$item => $remaining+1]);
            $final = $final->add($stuff);
        }

        /* calculate for the $remaining */

        /* edgecase for rounded down to 9s */
        $calcwithZeros = false;
        if(preg_match('/^[9]*$/',$remaining)) {
            $calcwithZeros = true;
            for( $i = $remainingWhichPowOf10; $i >= 0; $i-- ) {
                $stuff4 = new DigitCount(['0' => pow(10,$i)]);
                $final = $final->add($stuff4);
            }
        }else{
            if($remainingWhichPowOf10 > 0){
                $stuff3 = new DigitCount(['0' => pow(10,$remainingWhichPowOf10)]);
                $final = $final->add($stuff3);
            }
        }
        $final = $final->add($this->internalCalculate($remaining,$calcwithZeros));

        /* optionally take off zeros */
        if($withOutZeros) {
            $leadingZeros = $this->lzCalculator->calculate($number);
            $final = $final->subtract($leadingZeros);
        }

        return $final;
    }
}
