<?php

require_once 'vendor/autoload.php';


$calc = new \hip911\Hotel\Calculator\RecursiveDigitCountCalculator(
    new \hip911\Hotel\Calculator\BaseValueDigitCountCalculator(),
    new \hip911\Hotel\Calculator\LeadingZerosDigitCountCalculator()
);
var_dump($calc->calculate('200'));
var_dump($calc->calculate('5000'));
var_dump($calc->calculate('1000000000000'));
var_dump($calc->calculate('999999999999999999'));
var_dump($calc->calculate('1000000000000000000'));


//$bvcalc = new \hip911\Hotel\Calculator\BaseValueDigitCountCalculator();
//for($i = 1; $i < 20; $i++){
//    $allNines = implode('', array_fill(0, $i, '9'));
//    var_dump($bvcalc->calculate($allNines));
//}
